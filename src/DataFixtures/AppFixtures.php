<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\Order;
use App\Entity\Invoce;
use App\Entity\Product;
use App\Entity\OrderLine;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;
    private $product;

    public function __construct(UserPasswordEncoderInterface $encoder){
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        for ($p=0; $p < 25; $p++) { 
            $product = new Product;

            $product->setName("product n°" . ($p + 1))
                    ->setDescription($faker->sentence(6))
                    ->setPrice($faker->randomNumber(2));
            
            $manager->persist($product);
        }
        
        $manager->flush();

        $this->product = $manager->getRepository(Product::class)->findAll();

        for ($u = 0; $u < 5; $u++) {
            $user = new User;
            
            $hash = $this->encoder->encodePassword($user, 'password');
            
            $user->setEmail(($u === 0) ? "admin@admin.fr" : $faker->safeEmail())
                ->setPassword($hash)
                ->setName($faker->name(array_rand(array('male', 'female'))))
                ->setRoles([($u === 0) ? 'ROLE_ADMIN' : 'ROLE_USER']);
            
            $manager->persist($user);

            for ($o=0; $o < 10; $o++) { 
                $order = new Order;

                $order->setNum($o + 1)
                    ->setUser($user)
                    ->setCreatedAt(new \Datetime);

                $manager->persist($order);
                
                for ($ol=0; $ol < rand(2, 9); $ol++) { 
                    $orderLine = new OrderLine;
                    
                    $orderLine->setProduct($this->product[rand(0,24)])
                    ->setQuantity(rand(1, 7))
                    ->setPurchasOrder($order);
                    
                    $manager->persist($orderLine);
                }

                $invoce = new Invoce;

                $invoce->setPurchaseOrder($order)
                        ->setState("Payé")
                        ->setCreatedAt(new \Datetime);
                
                $manager->persist($invoce);

            }
        }
        
        $manager->flush();
    }
}



// un utilisateur possède 10 commandes

// Une commande possède entre 3 et 10 commande

// Une ligne de commande est associé à un produit

// une commande possède une facture asscocié