<?php

namespace App\DataPersister;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;

class InvocePersister implements DataPersisterInterface {
    protected $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    public function supports($data) : bool {
        return $data instanceof Order;
    }

    public function persist($data) {
        // Mettre un edate de création à notre domaine
        $data->setCreatedAt(new \DateTime());

        // Demander à doctrine de persister
        $this->manager->persist($data);
        $this->manager->flush();
    }

    public function remove($data) {
        $this->manager->remove($data);
        $this->manager->flush();
    }
}