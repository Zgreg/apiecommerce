<?php

namespace App\DataPersister;

use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use ApiPlatform\Core\DataPersister\DataPersisterInterface;

class OrderPersister implements DataPersisterInterface {
    protected $manager;

    public function __construct(EntityManagerInterface $manager) {
        $this->manager = $manager;
    }

    public function supports($data) : bool {
        return $data instanceof Invoce;
    }

    public function persist($data) {
        $lastNum = $manager->getRepository(Order::class)->findOneBy([], ['num' => 'desc']);

        $data->setCreatedAt(new \DateTime());

        $data->setNum($lastNum->getNum() + 1);

        $this->manager->persist($data);
        $this->manager->flush();
    }

    public function remove($data) {
        $this->manager->remove($data);
        $this->manager->flush();
    }
}