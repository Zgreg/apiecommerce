<?php
// , "user:read"
namespace App\Entity;

use App\Entity\Order;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\InvoceRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=InvoceRepository::class)
 * @ApiResource(
 *      normalizationContext={
 *          "groups" = {"invoce:read"}
 *      },
 * )
 */
class Invoce
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"invoce:read", "order:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Order::class, inversedBy="invoce", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $purchaseOrder;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"order:read", "user:read"})
     */
    private $state;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"invoce:read", "order:read", "user:read"})
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPurchaseOrder(): ?Order
    {
        return $this->purchaseOrder;
    }

    public function setPurchaseOrder(Order $purchaseOrder): self
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @Groups({"invoce:read", "order:read", "user:read"})
     * @return float
     */
    public function getTotalPrice() : float {
        $priceTotal = 0;
        $machin = $this->getPurchaseOrder()->getOrderLines()->toArray();
        foreach ($machin as $truc) {
            $priceTotal += $truc->getTotalProductOrderLine();
        }
        return $priceTotal;
    }
}
