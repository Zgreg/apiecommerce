<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Invoce;
use App\Entity\OrderLine;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\OrderRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 * @ApiResource(
 *      normalizationContext={
 *          "groups" = {"order:read"}
 *      },
 * )
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"order:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"order:read", "user:read"})
     */
    private $num;

    /**
     * @ORM\OneToMany(targetEntity=OrderLine::class, mappedBy="purchasOrder", orphanRemoval=true)
     * @Groups({"order:read", "user:read"})
     */
    private $orderLines;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="orders")
     * @Groups({"order:read"})
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"order:read", "user:read"})
     */
    private $createdAt;

    /**
     * @ORM\OneToOne(targetEntity=Invoce::class, mappedBy="purchaseOrder", cascade={"persist", "remove"})
     * @Groups({"order:read", "user:read"})
     */
    private $invoce;

    public function __construct()
    {
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNum(): ?int
    {
        return $this->num;
    }

    public function setNum(int $num): self
    {
        $this->num = $num;

        return $this;
    }

    /**
     * @return Collection|OrderLine[]
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines[] = $orderLine;
            $orderLine->setPurchasOrder($this);
        }

        return $this;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->removeElement($orderLine)) {
            // set the owning side to null (unless already changed)
            if ($orderLine->getPurchasOrder() === $this) {
                $orderLine->setPurchasOrder(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getInvoce(): ?Invoce
    {
        return $this->invoce;
    }

    public function setInvoce(Invoce $invoce): self
    {
        // set the owning side of the relation if necessary
        if ($invoce->getPurchaseOrder() !== $this) {
            $invoce->setPurchaseOrder($this);
        }

        $this->invoce = $invoce;

        return $this;
    }
}
