<?php

namespace App\Entity;

use App\Entity\Order;
use App\Entity\Product;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\OrderLineRepository;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=OrderLineRepository::class)
* @ApiResource(
 *      normalizationContext={
 *          "groups" = {"orderLine:read"}
 *      },
 * )
 */
class OrderLine
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"orderLine:read", "order:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class)
     * @Groups({"orderLine:read", "order:read", "user:read"})
     */
    private $product;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"orderLine:read", "order:read", "user:read"})
     */
    private $quantity;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderLines")
     * @ORM\JoinColumn(nullable=false)
     */
    private $purchasOrder;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPurchasOrder(): ?Order
    {
        return $this->purchasOrder;
    }

    public function setPurchasOrder(?Order $purchasOrder): self
    {
        $this->purchasOrder = $purchasOrder;

        return $this;
    }

    /**
     * @Groups({"orderLine:read", "order:read", "user:read"})
     *
     * @return float
     */
    public function getTotalProductOrderLine() : float
    {
        // dd($this->getQuantity(), $this->getProduct()->getPrice(), $this->getProduct()->getPrice() * $this->getQuantity());
        return $this->getProduct()->getPrice() * $this->getQuantity();
    }
}
